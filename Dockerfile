#
# Stage: build
#
FROM mcr.microsoft.com/dotnet/sdk:5.0 AS build

WORKDIR /src
COPY . .

RUN dotnet restore
RUN dotnet publish -c Release -o publish WebSocketTest1

#
# Stage: runtime
#
FROM mcr.microsoft.com/dotnet/aspnet:5.0 AS runtime

#install curl
RUN apt-get update
RUN apt-get install -y curl

EXPOSE	80/tcp
#EXPOSE	5001/tcp

WORKDIR /app
COPY --from=build /src/publish/ .

# Change timezone to local time
ENV TZ=America/Guyana
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

CMD ["dotnet", "WebSocketTest1.dll"]
