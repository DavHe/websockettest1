﻿const vm = new Vue({
    el: '#app',
    data: {
        socket: {},
        chatRoomMessage:[],

        userName: '',
        message:'',
    },
    created() {
        this.userName = 'User' + (new Date().getTime() % 10000);
                
        this.socket = new WebSocket(this.GetWebSocketUrl());
        this.socket.onopen = () => this.SendMessage("/USER " + this.userName);
       
        this.socket.onmessage = this.AcceptMessage;
    },
    methods: {
        GetWebSocketUrl() {
            let location = document.location;
            let port = location.port ? (':' + location.port) : '';
            let scheme = location.protocol === 'https:' ? 'wss' : 'ws';
            let wsUrl = scheme + '://' + location.hostname + port + '/ws';

            return wsUrl;
        }, 
        AcceptMessage(event) {
            console.log(event.data);
            this.chatRoomMessage.push(event.data);
        },
        SendMessage(msg) {
            if (this.socket && this.socket.readyState === WebSocket.OPEN)
                this.socket.send(msg);
        },
    }
});