using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.WebSockets;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Prometheus;
using WebSocketTest1.Handler;

namespace WebSocketTest1
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<WebSocketHandler>();
            services.AddRazorPages();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();
            //add static files like js,css,image
            app.UseStaticFiles();
            
            //Prometheus
            app.UseHttpMetrics();

            //Api Controller
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                //Prometheus
                endpoints.MapMetrics();
            });

            //Mvc Controller
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapRazorPages();
            });
            
            //加入 WebSocket 功能
            app.UseWebSockets(new WebSocketOptions
            {
                KeepAliveInterval = TimeSpan.FromSeconds(30)
            });
            
            app.Use(async (context, next) =>
            {
                if (context.Request.Path == "/ws")
                {
                    if (context.WebSockets.IsWebSocketRequest)
                    {
                        using (var ws = await context.WebSockets.AcceptWebSocketAsync())
                        {
                            var wsHandler = context.RequestServices.GetRequiredService<WebSocketHandler>();
                            await wsHandler.ProcessWebSocket(ws);
                        }
                    }
                    else 
                        context.Response.StatusCode = (int)System.Net.HttpStatusCode.BadRequest;
                }
                else await next();
            });
        }
    }
}